package ru.tsc.babeshko.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.babeshko.tm.api.service.IAuthService;
import ru.tsc.babeshko.tm.api.service.IServiceLocator;
import ru.tsc.babeshko.tm.dto.request.UserLoginRequest;
import ru.tsc.babeshko.tm.dto.request.UserLogoutRequest;
import ru.tsc.babeshko.tm.dto.request.UserProfileRequest;
import ru.tsc.babeshko.tm.dto.response.UserLoginResponse;
import ru.tsc.babeshko.tm.dto.response.UserLogoutResponse;
import ru.tsc.babeshko.tm.dto.response.UserProfileResponse;
import ru.tsc.babeshko.tm.dto.model.SessionDTO;
import ru.tsc.babeshko.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.babeshko.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final UserDTO user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}