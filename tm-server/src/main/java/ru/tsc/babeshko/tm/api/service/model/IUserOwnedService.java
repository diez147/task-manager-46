package ru.tsc.babeshko.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.model.AbstractUserOwnedModel;
import ru.tsc.babeshko.tm.model.User;

import java.util.Date;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @Nullable
    M create(@Nullable User user, @Nullable String name);

    @Nullable
    M create(@Nullable User user, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable User user,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}